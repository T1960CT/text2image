package main

import (
	"flag"
	"fmt"
	"image"
	"image/color"

	// "image/draw"
	"image/png"
	"os"
	"unicode/utf8"

	"golang.org/x/image/draw"
	"golang.org/x/image/font"
	"golang.org/x/image/font/basicfont"
	"golang.org/x/image/math/fixed"
)

// Discord chat window background in dark mode
var bgColour = color.RGBA{54, 57, 62, 255}
var fgColour = color.RGBA{255, 255, 255, 255}
var output = "/tmp/output.png"
var imgScale = 2

// font has built in line spacing, but can be increased through this
const lineSpace = 13
const fontWidth = 7
const fontHeight = 13
const leftPadding = 6

func init() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "How to use Text2Image:\n")
		flag.PrintDefaults()
		fmt.Fprintf(os.Stderr, "\nExample:\n\t"+`text2image -o=/tmp/output.png -s=2 "TITLE" "This is line 1" "This is line 2" "" "This is line 4"`+"\n")
	}
}

func main() {
	flag.StringVar(&output, "o", "/tmp/output.png", "Filepath and name to save to")
	flag.IntVar(&imgScale, "s", 1, "Image scale")
	flag.Parse()
	args := flag.Args()

	img := createImage(args)
	err := writeImage(img)
	if err != nil {
		panic(err)
	}

}

func sizeCanvas(lines []string) (w int, h int) {
	longestString := 0
	for _, v := range lines {
		c := utf8.RuneCountInString(v)
		if c > longestString {
			longestString = c
		}
	}

	w = (longestString * fontWidth) + (leftPadding * 2)
	h = (lineSpace * len(lines)) + lineSpace // add in one last line for footer spacing

	return
}

func createBackground(width int, height int, background color.RGBA) *image.RGBA {
	rect := image.Rect(0, 0, width, height)
	img := image.NewRGBA(rect)
	draw.Draw(img, img.Bounds(), &image.Uniform{background}, image.Point{}, draw.Src)

	return img
}

func lineByLine(img *image.RGBA, lines []string) *image.RGBA {
	lineTextVerticalAlign := ((lineSpace - fontHeight) / 2) + fontHeight
	for l, v := range lines {
		vPos := (lineSpace * l) + lineTextVerticalAlign

		// SRC: https://stackoverflow.com/a/38300583k
		d := &font.Drawer{
			Dst: img,
			Src: image.NewUniform(fgColour),
			// Face: basicfont.Face7x13,
			Face: basicfont.Face7x13,
			Dot:  fixed.Point26_6{X: fixed.Int26_6(leftPadding * 64), Y: fixed.Int26_6(vPos * 64)},
		}
		d.DrawString(v)
	}

	return img
}

func createImage(args []string) *image.RGBA {
	w, h := sizeCanvas(args)
	img := createBackground(w, h, bgColour)

	lineByLine(img, args)

	return img
}

func writeImage(img *image.RGBA) error {
	if output == "" {
		return fmt.Errorf("empty filepath, how did we get here?")
	}

	if output[len(output)-4:] != ".png" {
		output += ".png"
	}

	if imgScale > 1 {
		scaled := image.NewRGBA(image.Rect(0, 0, img.Bounds().Max.X*imgScale, img.Bounds().Max.Y*imgScale))
		draw.CatmullRom.Scale(scaled, scaled.Rect, img, img.Bounds(), draw.Over, nil)

		img = scaled
	}

	f, err := os.Create(output)
	if err != nil {
		return err
	}
	return png.Encode(f, img)
}
