# Text2Image

[![coverage report](https://gitlab.com/T1960CT/text2image/badges/master/coverage.svg)](https://gitlab.com/T1960CT/text2image/-/commits/master)

This program accepts a list of strings and turns them into a simple png.

[Download v1.0.0](https://gitlab.com/T1960CT/text2image/-/jobs/1535853255/artifacts/raw/text2image)

```sh
:~$ text2image -h

How to use Text2Image:
  -o string
        Filepath and name to save to (default "/tmp/output.png")
  -s int
        Image scale (default 1)

Example:
        text2image -o=/tmp/output.png -s=2 "SCALED x2" "This is line 1" "This is line 2" "" "This is line 4"
```
