package main

import (
	"image/color"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSizeCanvas(t *testing.T) {
	assert := assert.New(t)

	lines := []string{"test", "longest"}
	w, h := sizeCanvas(lines)
	assert.Equal(61, w)
	assert.Equal(39, h)
}

func TestCreateBackground(t *testing.T) {
	assert := assert.New(t)

	img := createBackground(10, 10, color.RGBA{0, 0, 0, 255})
	assert.Equal(10, img.Bounds().Max.X)
	assert.Equal(10, img.Bounds().Max.Y)
	assert.Equal(true, img.Opaque())
}

func TestAddLabel(t *testing.T) {
	assert := assert.New(t)

	img := createBackground(100, 100, color.RGBA{0, 0, 0, 255})
	imgManipulated := createBackground(100, 100, color.RGBA{0, 0, 0, 255})
	lineByLine(imgManipulated, []string{"Label"})

	assert.NotEqual(img, imgManipulated)
}

func TestCreateAndWriteImage(t *testing.T) {
	assert := assert.New(t)

	output = "/tmp/test.png"

	args := []string{"Label"}
	img := createImage(args)
	assert.NotNil(img)

	assert.NoError(writeImage(img))
	assert.FileExists(output)
	os.Remove(output)

	output = ""
	assert.Error(writeImage(img))

	output = "/tmp/test"
	assert.NoError(writeImage(img))
	// check to make sure writeImage updates the target filename
	assert.Equal("/tmp/test.png", output)
	assert.FileExists(output)
	os.Remove(output)

	output = "/deepspace/nine"
	assert.Error(writeImage(img))
}
